﻿using EnvDTE;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.CodeAnalysis;

namespace Code_Overlordeh
{
    public static class Kernel
    {
        public struct Class
        {
            public CodeClass cls;
            public SyntaxReference node;

            public Class(CodeClass cls, SyntaxReference node)
            {
                this.cls = cls;
                this.node = node;
            }

            public Element ToElement()
            {
                return new Element(cls as CodeElement, node);
            }
        }

        public struct Element
        {
            public CodeElement elem;
            public SyntaxReference node;

            public Element(CodeElement elem, SyntaxReference node)
            {
                this.elem = elem;
                this.node = node;
            }
        }

        static DTE dte;
        static SolutionEvents slnEvents;
        static TextEditorEvents txtEdEvents;
        static ProjectItemsEvents itemEvents;

        public static bool IsInitialized = false;

        public static SortedDictionary<string, Class> Classes { get; private set; }

        public delegate void OnChangedEventHandler(object sender, System.Windows.RoutedEventArgs e);
        public static event OnChangedEventHandler Changed;

        static bool Working;
        static IComponentModel componentModel;
        static Workspace workspace;

        public static void Initialize(DTE dte)
        {
            Kernel.dte = dte;

            IsInitialized = true;

            slnEvents = dte.Events.SolutionEvents;
            slnEvents.Opened += SolutionEvents_Opened;

            txtEdEvents = dte.Events.TextEditorEvents;
            txtEdEvents.LineChanged += TxtEdEvents_LineChanged; ;

            itemEvents = dte.Events.SolutionItemsEvents;
            itemEvents.ItemAdded += ItemEvents_ItemAdded;
            itemEvents.ItemRemoved += ItemEvents_ItemRemoved;

            componentModel = (IComponentModel)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SComponentModel));
            workspace = componentModel.GetService<Microsoft.VisualStudio.LanguageServices.VisualStudioWorkspace>();

            if (dte.Solution.IsOpen)
                SolutionEvents_Opened();
        }

        /// <summary>
        /// Loads each project item with LoadPorjectItem 
        /// </summary>
        /// <param name="proj"> The project to load </param>
        private static void LoadProject(EnvDTE.Project proj)
        {
            void RecurseProjectItems(ProjectItems proj_items)
            {
                foreach (ProjectItem item in proj_items)
                {
                    LoadProjectItem(item);

                    RecurseProjectItems(item.ProjectItems);
                }
            }

            Classes = new SortedDictionary<string, Class>();

            Working = true;
            RecurseProjectItems(proj.ProjectItems);
            Working = false;

            Changed?.Invoke(null, null);
        }

        /// <summary>
        /// Recurses through all containing items
        /// </summary>
        /// <param name="item"> The item to add </param>
        /// <param name="add"> Whether it adds or deletes the item </param>
        private static void LoadProjectItem(ProjectItem item, bool add = true)
        {
            void RecurseCodeElements(CodeElements codeElements, IEnumerator<SyntaxNode> nodes)
            {
                nodes.MoveNext();
                foreach (CodeElement elem in codeElements)
                {
                    System.Diagnostics.Trace.WriteLine(nodes.Current.ToString());
                    System.Diagnostics.Trace.WriteLine(elem.StartPoint.CreateEditPoint().GetText(elem.EndPoint));
                    switch (elem)
                    {
                        case CodeClass cls:
                            if (add)
                                Classes[cls.Name] = new Class(cls, nodes.Current.GetReference());
                            else
                                Classes.Remove(cls.Name);
                            break;
                        case CodeNamespace ns:
                            RecurseCodeElements(ns.Members, nodes.Current.ChildNodes().GetEnumerator());
                            break;
                        case CodeType ct:
                            RecurseCodeElements(ct.Members, nodes.Current.ChildNodes().GetEnumerator());
                            break;
                    }
                    nodes.MoveNext();
                }
            }

            if (item.FileCodeModel != null && item.Document != null)
            {
                var documentId = workspace.CurrentSolution.GetDocumentIdsWithFilePath(item.Document.FullName).FirstOrDefault();
                var document = workspace.CurrentSolution.GetDocument(documentId);
                document.TryGetSyntaxRoot(out SyntaxNode root);
                RecurseCodeElements(item.FileCodeModel.CodeElements, root.ChildNodes().GetEnumerator());
            }

            if (!Working)
                Changed?.Invoke(null, null);
        }

        public static void SolutionEvents_Opened()
        {
            if (dte.Solution.Projects.OfType<EnvDTE.Project>().Any())
                LoadProject(dte.Solution.Projects.OfType<EnvDTE.Project>().First());
        }

        private static void TxtEdEvents_LineChanged(TextPoint StartPoint, TextPoint EndPoint, int Hint)
        {
            // TODO : an elegant way to load change
        }

        /// <summary>
        /// Adds the item to the dictionary after it was added to the project
        /// </summary>
        /// <param name="ProjectItem"> Item to add </param>
        private static void ItemEvents_ItemAdded(ProjectItem ProjectItem)
        {
            LoadProjectItem(ProjectItem);
        }

        /// <summary>
        /// Removess the item to the dictionary after it was removed to the project
        /// </summary>
        /// <param name="ProjectItem"> Item to remove </param>
        private static void ItemEvents_ItemRemoved(ProjectItem ProjectItem)
        {
            LoadProjectItem(ProjectItem, false);
        }

        /// <summary>
        /// Opens an editor window, highlighting the point's line
        /// </summary>
        /// <param name="item"></param>
        /// <param name="point"></param>
        public static void OpenWindowAt(ProjectItem item, TextPoint point)
        {
            Window wnd = item.Open();
            wnd.Activate();
            ((TextSelection)wnd.Selection).GotoLine(point.Line, true);
        }

        #region Retrieval

        public static string[] Class_Names => Classes.Keys.ToArray();


        /// <summary>
        /// Gets all the elements of type "elementType" from within the given CodeClass
        /// </summary>
        /// <typeparam name="T"> The type of a subclass of CodeElement </typeparam>
        /// <param name="cls"> The class from which to get the elements </param>
        /// <returns> A collection of CodeElements </returns>
        public static IEnumerable<T> GetClassElement<T>(CodeClass cls)
        {
            foreach (CodeElement elem in cls.Members)
                if (elem is T res)
                    yield return res;
        }


        public class ClassInheritanceNode
        {
            public CodeClass cls;
            public ClassInheritanceNode parent;
            public List<ClassInheritanceNode> children;
            public int depth;
            public int MaxDepth
            {
                get {
                    int GetMaxDepth(ClassInheritanceNode cur)
                    {
                        if (cur.children == null)
                            return cur.depth;

                        int max = 0;
                        foreach(ClassInheritanceNode child in cur.children)
                        {
                            int local_m_depth = GetMaxDepth(child);
                            if (local_m_depth > max)
                                max = local_m_depth;
                        }

                        return max;
                    }

                    return GetMaxDepth(this);
                }
            }

            public System.Windows.Point position;
            public System.Windows.Controls.Button button;
            public System.Windows.Shapes.Line line;

            public ClassInheritanceNode(CodeClass cls, ClassInheritanceNode parent = null, IEnumerable<ClassInheritanceNode> children = null, int depth = 0, bool isRoot = false)
            {
                this.cls = cls;
                this.parent = parent;
                this.children = children?.ToList();
                this.depth = depth;
            }

            public static void Assimilate(ClassInheritanceNode reciever, ClassInheritanceNode donor)
            {
                if (donor.children == null)
                    return;

                if (reciever.children == null)
                {
                    reciever.children = donor.children;
                    foreach (ClassInheritanceNode child in reciever.children)
                        child.parent = reciever;
                    return;
                }

                foreach (ClassInheritanceNode child in donor.children)
                {
                    int index = reciever.children.FindIndex(x => x == child);
                    if (index == -1)
                    {
                        child.parent = reciever;
                        reciever.children.Add(child);
                    }
                    else
                        Assimilate(reciever.children[index], child);
                }                     

            }

            public static void IncrementDepth(ClassInheritanceNode node)
            {
                node.depth += 1;
                if (node.children != null)
                    foreach (ClassInheritanceNode child in node.children)
                        IncrementDepth(child);
            }

            public static bool operator ==(ClassInheritanceNode a, ClassInheritanceNode b)
            {
                if (ReferenceEquals(a, null))
                    return ReferenceEquals(b, null);
                else if (ReferenceEquals(b, null))
                    return false;

                return a.cls == b.cls;
            }

            public static bool operator !=(ClassInheritanceNode a, ClassInheritanceNode b)
            {
                return !(a == b);
            }
        }

        public static ClassInheritanceNode[] GetClassInheritance()
        {
            CodeClass GetBaseClass(CodeClass possibleRoot)
            {
                foreach(CodeElement _base in possibleRoot.Bases)
                    if (_base is CodeClass cls_base)
                        if (Classes.Values.Any(x => x.cls == cls_base))
                            return cls_base;
                return null;
            }

            List<ClassInheritanceNode> roots = new List<ClassInheritanceNode>();

            ClassInheritanceNode UpwardsDFS(ClassInheritanceNode node)
            {
                CodeClass cls = GetBaseClass(node.cls);

                if(cls == null)
                    return node;

                ClassInheritanceNode parent = new ClassInheritanceNode(cls, children: new List<ClassInheritanceNode>() { node });
                node.parent = parent;
                ClassInheritanceNode.IncrementDepth(node);

                return UpwardsDFS(parent);
            }

            foreach (CodeClass cls in Classes.Values.Select(x => x.cls))
                if (GetBaseClass(cls) != null)
                {
                    ClassInheritanceNode node = new ClassInheritanceNode(cls);
                    ClassInheritanceNode root = UpwardsDFS(node);
                    int index = roots.FindIndex(x => x == root);
                    if (index == -1)
                        roots.Add(root);
                    else
                        ClassInheritanceNode.Assimilate(roots[index], root);
                }
                else
                    roots.Add(new ClassInheritanceNode(cls, isRoot: true));

            return roots.ToArray();
        }

        #endregion

        #region Creation

        public static void AddMethod(string path, string name, string type, vsCMAccess access, object position)
        {
            Classes[path.Split('\\').Last()].cls.AddFunction(name, vsCMFunction.vsCMFunctionFunction, type, position, access, null);
        }

        public static void AddField(string path, string name, string type, vsCMAccess access, object position)
        {
            Classes[path.Split('\\').Last()].cls.AddVariable(name, type, position, access, null);
        }

        #endregion

        #region Deletition

        public static void DeleteCodeElement(CodeElement elem)
        {
            EditPoint editPoint = elem.StartPoint.CreateEditPoint();
            editPoint.Delete(elem.EndPoint);
        }

        #endregion

        public static async void AsyncRename(Element elem, string newName)
        {
            var documentId = workspace.CurrentSolution.GetDocumentIdsWithFilePath(elem.elem.ProjectItem.Document.FullName).FirstOrDefault();
            var document = workspace.CurrentSolution.GetDocument(documentId);

            int pos = elem.node.Span.Start;
            ISymbol symbol = null;
            while (pos <= elem.node.Span.End)
            {
                symbol = await Microsoft.CodeAnalysis.FindSymbols.SymbolFinder.FindSymbolAtPositionAsync(document, pos);
                if (symbol != null)
                    break;
                System.Diagnostics.Trace.WriteLine(elem.elem.StartPoint.AbsoluteCharOffset + " " + elem.node.Span.Start);
                pos++;
            }
            var newSol = await Microsoft.CodeAnalysis.Rename.Renamer.RenameSymbolAsync(workspace.CurrentSolution, symbol, newName, workspace.Options);
            workspace.TryApplyChanges(newSol);
        }

    }
}
