﻿//------------------------------------------------------------------------------
// <copyright file="MainToolWindowControl.xaml.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System.Windows.Input;

namespace Code_Overlordeh
{
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;
    using System.Linq;
    using EnvDTE;
    using System.Collections.Generic;
    using System;
    using System.Windows.Shapes;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for MainToolWindowControl.
    /// </summary>
    public partial class MainToolWindowControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainToolWindowControl"/> class.
        /// </summary>
        public MainToolWindowControl()
        {
            this.InitializeComponent();

            Refresh(null, null);

            Loaded += Refresh;
            Kernel.Changed += Refresh;
        }

        Button selectedButton;
        Kernel.ClassInheritanceNode selected;

        Dictionary<string, CodeElement> class_elements = new Dictionary<string, CodeElement>();
        TextBox declaration;
        private void Refresh(object sender, RoutedEventArgs e)
        {
            const int btnWidth = 150;
            const int btnHeight = 35;

            void UpdateCodeElements(CodeClass cls)
            {
                void CreateMemberButton(CodeElement elem, string Content, string DocComment)
                {
                    void ShowDefinition(Button b, string declaration)
                    {
                        ClassMembers.Children.Remove(this.declaration);

                        if(this.declaration != null && this.declaration.Text as string == declaration)
                        {
                            this.declaration = null;
                            return;
                        }
                        this.declaration = new TextBox() { Text = declaration, Foreground = Brushes.DarkGray, BorderBrush = null, Focusable = false, TextWrapping = TextWrapping.WrapWithOverflow };
                        ClassMembers.Children.Insert(
                            ClassMembers.Children.IndexOf(b),
                            this.declaration);
                    }

                    Button btn = new Button()
                    {
                        Name = "cls_elem" + Guid.NewGuid().ToString().Replace("-", string.Empty),
                        Content = Content,
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        BorderBrush = null
                    };
                    btn.ToolTip = (string.IsNullOrEmpty(DocComment) ? null : DocComment);


                    class_elements.Add(btn.Name, elem);

                    btn.Click += delegate
                    {
                        EditPoint ep = elem.StartPoint.CreateEditPoint();
                        while (!ep.AtEndOfLine)
                            ep.CharRight();
                        ShowDefinition(btn, elem.StartPoint.CreateEditPoint().GetText(ep));
                    };

                    ClassMembers.Children.Add(btn);

                    var deleteItem = new MenuItem { Header = "Delete..." };

                    var goToDefinitionItem = new MenuItem { Header = "Go to definition" };

                    deleteItem.Click += delegate
                    {
                        Kernel.DeleteCodeElement(elem);
                        UpdateCodeElements(cls);
                    };

                    goToDefinitionItem.Click += delegate
                    {
                        Kernel.OpenWindowAt(cls.ProjectItem, elem.StartPoint);
                    };

                    btn.ContextMenu = new ContextMenu();
                    btn.ContextMenu.Items.Add(deleteItem);
                    btn.ContextMenu.Items.Add(goToDefinitionItem);
                }

                void CreateInterfaceButton(CodeInterface intf)
                {
                    Button btn = new Button()
                    {
                        Name = "cls_intf" + Guid.NewGuid().ToString().Replace("-", string.Empty),
                        Content = intf.Name,
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        BorderBrush = null,
                        ToolTip = intf.DocComment
                    };

                    class_elements.Add(btn.Name, intf as CodeElement);

                    Interfaces.Children.Add(btn);

                    var removeImpl = new MenuItem { Header = "Remove implementation" };

                    var goToDefinitionItem = new MenuItem { Header = "Go to definition" };

                    removeImpl.Click += delegate
                    {
                        cls.RemoveInterface(intf);

                        UpdateCodeElements(cls);
                    };

                    goToDefinitionItem.Click += delegate
                    {
                        Kernel.OpenWindowAt(cls.ProjectItem, cls.StartPoint);
                    };

                    btn.ContextMenu = new ContextMenu();
                    btn.ContextMenu.Items.Add(removeImpl);
                    btn.ContextMenu.Items.Add(goToDefinitionItem);
                }

                #region Update interfaces
                Interfaces.Children.Clear();

                foreach (var intf in cls.ImplementedInterfaces.OfType<CodeInterface>())
                    CreateInterfaceButton(intf);
                #endregion

                #region Update members
                void AddHeader(string text) =>
                    ClassMembers.Children.Add(new Label() { Content = text, Foreground = Brushes.DarkOliveGreen, BorderBrush = null });

                ClassMembers.Children.Clear();

                #region Update fields
                AddHeader("-fields");
                var vars = Kernel.GetClassElement<CodeVariable>(cls);
                foreach (CodeVariable var in vars)
                    CreateMemberButton(var as CodeElement, $"{var.Type.AsString} {var.Name}", var.DocComment);
                #endregion

                #region Update properties
                AddHeader("-properties");
                var props = Kernel.GetClassElement<CodeProperty>(cls);
                foreach (CodeProperty prp in props)
                {
                    var getsetstr = $"{(prp.Getter == null ? "" : "get;")}{(prp.Setter == null ? "" : "set;")}";
                    CreateMemberButton(prp as CodeElement, $"{prp.Type.AsString} {prp.Name} {{{getsetstr}}}", prp.DocComment);
                }
                #endregion

                #region Update methods
                AddHeader("-methods");
                var fncs = Kernel.GetClassElement<CodeFunction>(cls);
                foreach (CodeFunction fnc in fncs)
                {
                    var sigArgs = fnc.Parameters.OfType<CodeParameter>().Select(p => $"{p.Type.AsString} {p.Name}");
                    CreateMemberButton(fnc as CodeElement, $"{fnc.Type.AsString} {fnc.Name}({string.Join(", ", sigArgs)})", fnc.DocComment);
                }
                #endregion

                #region Update enums
                AddHeader("-enums");
                var enums = Kernel.GetClassElement<CodeEnum>(cls);
                foreach (CodeEnum en in enums)
                    CreateMemberButton(en as CodeElement, en.Name, en.DocComment);
                #endregion

                #region Update structs
                AddHeader("-structs");
                var structs = Kernel.GetClassElement<CodeStruct>(cls);
                foreach (CodeStruct st in structs)
                    CreateMemberButton(st as CodeElement, st.Name, st.DocComment);
                #endregion

                #endregion
            }

            Button CreateClassButton(Kernel.ClassInheritanceNode node, Point pos)
            {
                Button btn = new Button
                {
                    Content = node.cls.Name,
                    Width = btnWidth,
                    Height = btnHeight,
                    Background = Brushes.DarkGray,
                    BorderBrush = null
                };
                btn.ToolTip = (string.IsNullOrEmpty(node.cls.DocComment) ? null : node.cls.DocComment);

                btn.Click += delegate
                {
                    class_elements.Clear();

                    if (selectedButton == null)
                    {
                        selectedButton = btn;
                        selected = node;
                        selectedButton.Background = Brushes.DeepSkyBlue;
                    }
                    else if (selectedButton == btn)
                        DeselectClass();
                    else
                    {
                        DeselectClass();
                        selectedButton = btn;
                        selected = node;
                        selectedButton.Background = Brushes.DeepSkyBlue;
                    }

                    if (selectedButton != null)
                    {
                        UpdateCodeElements(node.cls);

                        Expander_Grid.Visibility = Visibility.Visible;

                        Kernel.ClassInheritanceNode auxNode = node;
                        while (auxNode != null)
                        {
                            if (auxNode.line != null)
                                auxNode.line.Stroke = Brushes.OrangeRed;

                            auxNode = auxNode.parent;
                        }

                        EditPoint ep = node.cls.StartPoint.CreateEditPoint();
                        while (ep.GetText(1) != "{" && !ep.AtEndOfLine)
                            ep.CharRight();
                        ClassDefinition.Text = node.cls.StartPoint.CreateEditPoint().GetText(ep);
                    }
                    else
                        Expander_Grid.Visibility = Visibility.Hidden;
                };

                var addNewItem = new MenuItem { Header = "Add New..." };

                var renameItem = new MenuItem { Header = "Rename" };

                var goToDefinitionItem = new MenuItem { Header = "Go to definition" };

                addNewItem.Click += delegate
                {
                    var addNew = new AddNewDialog();
                    addNew.SelectClass(node.cls);

                    addNew.Activate();
                    addNew.Show();
                };

                renameItem.Click += delegate
                {
                    var rename = new RenameDialog();
                    rename.SetOldName(Kernel.Classes.Values.First(x => x.cls == node.cls).ToElement());

                    rename.Activate();
                    rename.Show();
                };

                goToDefinitionItem.Click += delegate
                {
                    Kernel.OpenWindowAt(node.cls.ProjectItem, node.cls.StartPoint);
                };

                btn.ContextMenu = new ContextMenu();
                btn.ContextMenu.Items.Add(addNewItem);
                btn.ContextMenu.Items.Add(renameItem);
                btn.ContextMenu.Items.Add(goToDefinitionItem);

                Contents.Children.Add(btn);

                Canvas.SetLeft(btn, pos.X);
                Canvas.SetTop(btn, pos.Y);

                return btn;
            }

            if (Kernel.Classes == null)
            {
                Kernel.SolutionEvents_Opened();
                if (Kernel.Classes == null)
                    return;
            }

            #region Draw Inheritance View

            Contents.Children.Clear();

            Point curPos = new Point(Contents.ActualWidth / 2, 50);
            double midX = Contents.ActualWidth / 2;
            double pos_step_x = 175;
            double pos_step_y = 60;

            Kernel.ClassInheritanceNode[] inheritance = Kernel.GetClassInheritance();

            int width = 0;
            void GetDepthWidth(Kernel.ClassInheritanceNode node, int targetDepth)
            {
                if (node.depth == 0)
                    width = 0;

                if (node.depth != targetDepth)
                {
                    if (node.children == null)
                        return;
                    foreach (Kernel.ClassInheritanceNode child in node.children)
                        GetDepthWidth(child, targetDepth);
                    return;
                }

                width++;
            }

            void AddDepth(Kernel.ClassInheritanceNode node, int targetDepth, bool parents = false)
            {
                if (node.button != null)
                    return;

                if (node.depth != targetDepth)
                {
                    if (node.children == null)
                        return;
                    foreach (Kernel.ClassInheritanceNode child in node.children)
                        AddDepth(child, targetDepth, parents);
                    return;
                }

                if (parents && node.children == null)
                    return;
                if (parents)
                    curPos.X = node.children.First().position.X + ((node.children.Last().position.X - node.children.First().position.X) / 2);

                node.button = CreateClassButton(node, curPos);
                node.position = new Point(curPos.X, curPos.Y);
                curPos.X += pos_step_x;

                if (parents && node.parent != null && !node.parent.children.Any(x => x.children != null && x.button == null))
                    foreach (Kernel.ClassInheritanceNode sibling in node.parent.children)
                    {
                        if (sibling.button != null)
                            continue;

                        sibling.button = CreateClassButton(sibling, curPos);
                        sibling.position = new Point(curPos.X, curPos.Y);
                        curPos.X += pos_step_x;
                    }
            }

            void GetDepthLastX(Kernel.ClassInheritanceNode node, int targetDepth)
            {
                if (node.depth != targetDepth)
                {
                    if (node.children == null)
                        return;
                    foreach (Kernel.ClassInheritanceNode child in node.children)
                        GetDepthLastX(child, targetDepth);
                    return;
                }

                curPos.X = (node.position.X > curPos.X ? node.position.X : curPos.X);
            }

            void Reshape(Kernel.ClassInheritanceNode node, double offset)
            {
                node.position.X -= offset;
                Canvas.SetLeft(node.button, node.position.X);
                if (node.children != null)
                    foreach (Kernel.ClassInheritanceNode child in node.children)
                    {
                        Reshape(child, offset);

                        Line line = new Line()
                        {
                            X1 = node.position.X + btnWidth / 2,
                            Y1 = node.position.Y + btnHeight,
                            X2 = child.position.X + btnWidth / 2,
                            Y2 = child.position.Y,
                            Stroke = Brushes.DarkTurquoise,
                            StrokeThickness = 4
                        };
                        child.line = line;
                        Contents.Children.Add(line);
                    }
            }

            double maxWidth = 0;
            foreach (var root in inheritance)
            {
                int depthToGo = root.MaxDepth;
                curPos.Y += depthToGo * pos_step_y;
                double auxPosY = curPos.Y;
                for (int depth = depthToGo; depth >= 0; depth--)
                {
                    GetDepthWidth(root, depth);

                    curPos.X = 0;
                    AddDepth(root, depth, true);

                    curPos.X = 0;
                    GetDepthLastX(root, depth);
                    curPos.X += pos_step_x;

                    AddDepth(root, depth);

                    maxWidth = (curPos.X > maxWidth ? curPos.X : maxWidth);
                    curPos.Y -= pos_step_y;
                }
                curPos.Y = auxPosY + pos_step_y;
            }


            Contents.Width = maxWidth + pos_step_x;
            Contents.Height = curPos.Y;

            double anchorX = inheritance.Max(x => x.position.X) - pos_step_x / 2;
            foreach (var root in inheritance)
                Reshape(root, root.position.X - anchorX);

            #endregion
        }

        private void DeselectClass()
        {
            Expander_Grid.Visibility = Visibility.Collapsed;
            if (selectedButton != null)
                selectedButton.Background = Brushes.DarkGray;
            while (selected != null)
            {
                if (selected.line != null)
                    selected.line.Stroke = Brushes.DarkTurquoise;

                selected = selected.parent;
            }
            selected = null;
            selectedButton = null;
        }

        private void GridSplitter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            double xadjust = Expander_Grid.ActualWidth + e.HorizontalChange;

            if (xadjust >= Expander_Grid.MinWidth)
                Expander_Grid.Width = xadjust;
        }

        private double draggedDistance;
        private Point? lastDragPoint;

        private void moveContent(Vector vec)
        {
            ContentScroll.ScrollToHorizontalOffset(ContentScroll.HorizontalOffset - vec.X);
            ContentScroll.ScrollToVerticalOffset(ContentScroll.VerticalOffset - vec.Y);
        }

        private void ContentScroll_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var mousePos = e.GetPosition(ContentScroll);
            if (mousePos.X <= ContentScroll.ViewportWidth && mousePos.Y < ContentScroll.ViewportHeight)
            {
                ContentScroll.Cursor = Cursors.SizeAll;
                lastDragPoint = mousePos;
                Mouse.Capture(ContentScroll);
            }
        }

        private void ContentScroll_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (lastDragPoint.HasValue)
            {
                var posNow = e.GetPosition(ContentScroll);

                draggedDistance += (posNow - lastDragPoint.Value).Length;
                moveContent(posNow - lastDragPoint.Value);

                lastDragPoint = posNow;
            }
        }

        private void ContentScroll_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (draggedDistance < 5)
                DeselectClass();

            ContentScroll.Cursor = Cursors.Arrow;
            ContentScroll.ReleaseMouseCapture();
            lastDragPoint = null;
            draggedDistance = 0;
        }

        private void Code_Overlordeh_KeyUp(object sender, KeyEventArgs e)
        {
            // Refresh
            if(e.Key == Key.F5)
                Kernel.SolutionEvents_Opened();
        }

        private void RefreshAll(object sender, RoutedEventArgs e)
            => Kernel.SolutionEvents_Opened();
    }
}