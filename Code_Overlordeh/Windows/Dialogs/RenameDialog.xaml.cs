﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Code_Overlordeh
{
    /// <summary>
    /// Interaction logic for RenameDialog.xaml
    /// </summary>
    public partial class RenameDialog : Window
    {
        public RenameDialog()
        {
            InitializeComponent();
        }

        Kernel.Element elem;

        public void SetOldName(Kernel.Element elem)
        {
            this.elem = elem;
            OldName.Text = elem.elem.Name;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Kernel.AsyncRename(elem, NewName.Text);
        }
    }
}
