﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using EnvDTE;

namespace Code_Overlordeh
{
    /// <summary>
    /// Interaction logic for AddNew.xaml
    /// </summary>
    public partial class AddNewDialog : System.Windows.Window
    {
        string selectedCategory;

        Dictionary<string, vsCMAccess> access = new Dictionary<string, vsCMAccess>();
        Dictionary<string, object> positions;

        public AddNewDialog()
        {
            InitializeComponent();
            foreach(ComboBoxItem item in Access.Items)
                switch(item.Content as string)
                {
                    case "default":
                        access.Add("default", vsCMAccess.vsCMAccessDefault);
                        break;
                    case "private":
                        access.Add("private", vsCMAccess.vsCMAccessPrivate);
                        break;
                    case "public":
                        access.Add("public", vsCMAccess.vsCMAccessPublic);
                        break;
                    case "protected":
                        access.Add("protected", vsCMAccess.vsCMAccessProtected);
                        break;
                    default:
                        break;
                }

            grid.Visibility = Visibility.Hidden;
        }

        void ResetPositions()
        {
            positions = new Dictionary<string, object>();
            Position.Items.Clear();

            Position.Items.Add(new ComboBoxItem() { Content = "start" });
            positions.Add("start", 0);

            Position.Items.Add(new ComboBoxItem() { Content = "end" });
            positions.Add("end", -1);
        }

        /// <summary>
        /// The class in which the addition will be made
        /// </summary>
        /// <param name="cls"></param>
        public void SelectClass(CodeClass cls)
        {
            Class.IsEnabled = false;

            Path.Text = string.Join(@"\", cls.ProjectItem.ContainingProject.Name, cls.ProjectItem.Document.Name, cls.Name);
        }

        Button lastSelectedCategoryButton;
        private void SelectCategory_ButtonClick(object sender, RoutedEventArgs e)
        {
            grid.Visibility = Visibility.Visible;

            Button _sender = (sender as Button);
            selectedCategory = _sender.Content as string;

            ResetPositions();
            switch(selectedCategory)
            {
                case "Method":
                    IEnumerable<CodeFunction> fncs = Kernel.GetClassElement<CodeFunction>(Kernel.Classes[Path.Text.Split('\\').Last()].cls);
                    foreach (CodeFunction fnc in fncs)
                    {
                        Position.Items.Add(new ComboBoxItem() { Content = fnc.Name });
                        positions.Add(fnc.Name, fnc);
                    }
                    break;
                case "Field":
                    IEnumerable<CodeVariable> fields = Kernel.GetClassElement<CodeVariable>(Kernel.Classes[Path.Text.Split('\\').Last()].cls);
                    foreach (CodeVariable field in fields)
                    {
                        Position.Items.Add(new ComboBoxItem() { Content = field.Name });
                        positions.Add(field.Name, field);
                    }
                    break;
                default:
                    break;
            }

            if (lastSelectedCategoryButton != null)
                lastSelectedCategoryButton.Background = null;

            _sender.Background = Brushes.CornflowerBlue;
            lastSelectedCategoryButton = _sender;
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            switch(selectedCategory)
            {
                case "Method":
                    Kernel.AddMethod(Path.Text, 
                        Name.Text, 
                        type.Text,
                        access[(Access.SelectedItem as ComboBoxItem).Content as string],
                        positions[(Position.SelectedItem as ComboBoxItem).Content as string]);
                    break;
                case "Field":
                    Kernel.AddField(Path.Text,
                        Name.Text,
                        type.Text,
                        access[(Access.SelectedItem as ComboBoxItem).Content as string],
                        positions[(Position.SelectedItem as ComboBoxItem).Content as string]);
                    break;
                default:
                    break;
            }
        }
    }
}
